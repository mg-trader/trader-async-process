package eu.verante.trader.util.asyncprocess;

public interface AsyncProcessStatusChecker {
    AsyncProcess getCurrentStatus();
}
