package eu.verante.trader.util.asyncprocess;

import lombok.Builder;
import lombok.Data;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.UUID;

@Data
@Builder
public class AsyncProcess {

    private UUID processId;

    private ProcessStatus status;

    private int progress;
    private int total;

    private ZonedDateTime requestedAt;

    private ZonedDateTime completedAt;

    private Duration duration;

    public static AsyncProcess empty() {
        return AsyncProcess.builder()
                .processId(UUID.randomUUID())
                .status(ProcessStatus.PROCESSING)
                .requestedAt(ZonedDateTime.now())
                .progress(0)
                .total(0)
                .build();
    }
}
