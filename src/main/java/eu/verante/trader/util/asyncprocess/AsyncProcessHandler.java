package eu.verante.trader.util.asyncprocess;

public interface AsyncProcessHandler<R> {

    void process(R request, AsyncProcess process);
}
