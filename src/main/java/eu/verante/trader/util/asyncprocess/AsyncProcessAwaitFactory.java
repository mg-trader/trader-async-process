package eu.verante.trader.util.asyncprocess;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.Future;

@Component
public class AsyncProcessAwaitFactory {

    private final Set<MonitoredAsyncProcess> monitoredProcesses = new CopyOnWriteArraySet<>();

    public Future<ProcessStatus> createFor(AsyncProcessStatusChecker checker) {
        CompletableFuture<ProcessStatus> future = new CompletableFuture<>();
        monitoredProcesses.add(MonitoredAsyncProcess.of(future, checker));
        return future;
    }

    @Scheduled(fixedDelay = 1000)
    public void checkMonitoredProcesses() {
        Set<MonitoredAsyncProcess> completedProcesses = new HashSet<>();
        for (MonitoredAsyncProcess entry : monitoredProcesses) {
            AsyncProcess process = entry.statusChecker.getCurrentStatus();
            if (isCompleted(process)) {
                entry.future.complete(process.getStatus());
                completedProcesses.add(entry);
            }
        }
        monitoredProcesses.removeAll(completedProcesses);
    }

    private boolean isCompleted(AsyncProcess process) {
        return process.getCompletedAt() != null;
    }
}
