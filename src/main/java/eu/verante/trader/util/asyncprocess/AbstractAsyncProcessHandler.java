package eu.verante.trader.util.asyncprocess;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.scheduling.annotation.Async;

import java.time.Duration;
import java.time.ZonedDateTime;

public abstract class AbstractAsyncProcessHandler<R> implements AsyncProcessHandler<R>{

    private final Log logger = LogFactory.getLog(this.getClass());
    @Async("asyncProcessTaskExecutor")
    @Override
    public final void process(R request, AsyncProcess process) {
        try {

            doProcess(request, process);

            completeProcess(process);
        } catch (Exception e) {
            completeWithError(process, e);
            throw new RuntimeException(e);

        }
    }

    protected abstract void doProcess(R request, AsyncProcess process);

    private void completeProcess(AsyncProcess process) {
        process.setStatus(ProcessStatus.COMPLETED);
        process.setCompletedAt(ZonedDateTime.now());
        process.setDuration(Duration.between(process.getRequestedAt(), process.getCompletedAt()));

        logger.info("Management process completed: " + process.getProcessId());
    }

    private void completeWithError(AsyncProcess process, Exception e) {
        process.setStatus(ProcessStatus.ERROR);
        process.setCompletedAt(ZonedDateTime.now());
        process.setDuration(Duration.between(process.getRequestedAt(), process.getCompletedAt()));

        logger.error("Updating candles error", e);
    }
}
