package eu.verante.trader.util.asyncprocess;

public enum ProcessStatus {

    PROCESSING,
    COMPLETED,
    ERROR
}
