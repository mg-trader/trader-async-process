package eu.verante.trader.util.asyncprocess;

import java.util.concurrent.CompletableFuture;

class MonitoredAsyncProcess {
    final CompletableFuture<ProcessStatus> future;
    final AsyncProcessStatusChecker statusChecker;

    private MonitoredAsyncProcess(CompletableFuture<ProcessStatus> future, AsyncProcessStatusChecker statusChecker) {
        this.future = future;
        this.statusChecker = statusChecker;
    }

    static MonitoredAsyncProcess of(CompletableFuture<ProcessStatus> future, AsyncProcessStatusChecker checker) {
        return new MonitoredAsyncProcess(future, checker);
    }

}
