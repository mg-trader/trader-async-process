package eu.verante.trader.util.asyncprocess;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
public class AsyncProcessManager {
    private final Log logger = LogFactory.getLog(this.getClass());

    private final Map<UUID, AsyncProcess> processes = new HashMap<>();
    private final Map<Object, UUID> requests = new HashMap<>();

    public synchronized <R> AsyncProcess runProcess(R request, AsyncProcessHandler<R> handler) {
        if (requests.containsKey(request)) {
            AsyncProcess asyncProcess = processes.get(requests.get(request));
            if (asyncProcess.getStatus() == ProcessStatus.PROCESSING) {
                logger.info("reusing process for request: " + request);
                return asyncProcess;
            }
        }
        AsyncProcess process = AsyncProcess.empty();
        this.requests.put(request, process.getProcessId());
        this.processes.put(process.getProcessId(), process);
        handler.process(request, process);
        return process;
    }

    public AsyncProcess getById(UUID id) {
        return this.processes.get(id);
    }
}
