package eu.verante.trader.util.asyncprocess;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Configuration
public class AsyncConfiguration {

    @Bean()
    public Executor asyncProcessTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(Runtime.getRuntime().availableProcessors()*10);
        executor.setMaxPoolSize(Runtime.getRuntime().availableProcessors()*10);
        executor.setThreadNamePrefix("AsyncProcess-");
        executor.initialize();
        return executor;
    }
}
